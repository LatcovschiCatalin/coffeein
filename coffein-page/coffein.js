var price, whole_price, times, carts = [], products, allPrice;
slug = localStorage.getItem(`slugs`).toString();
let p = 0;

function onloadPrice() {
    if (parseInt(localStorage.getItem('whole-price')) > 0) {
        document.getElementById('whole-price-products').style.display = "block";
        document.getElementById('whole-price').innerHTML = whole_price;
        document.getElementById('times').innerHTML = times;

    } else {
        document.getElementById('whole-price-products').style.display = "none";
    }


}

if (parseInt(localStorage.getItem('whole-price')) > 0) {
    whole_price = parseInt(localStorage.getItem('whole-price'));
    times = parseInt(localStorage.getItem('times'));
} else {
    whole_price = 0;
    times = 0;
}

function getCartContent(){
    document.getElementById('background-menu').innerHTML = `${changeLanguage.CART}`;
}
function getFinishCommand(){
    html=`    <div class="finish-command" onclick="getPopupCart(); getMap2()">
        <h1>${changeLanguage.FINISH_ORDER}</h1>
    </div>
    <div class="follow-instagram">
        <h1>${changeLanguage.FOLLOW_US}</h1>
    </div>
</div>
`
    document.getElementById('finish-command').innerHTML = html;
}
function getPrice(id, t, sl) {
    document.getElementById('whole-price-products').style.display = "block";
    $.ajax({
        url: `http://api.coffeein.md/api/products`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {
            for (let j = 1; j <= parseInt(res.totalPages); j++) {
                $.ajax({
                    url: `http://api.coffeein.md/api/products?page=${j}`,
                    type: 'Get',
                    dataType: 'json',
                    success: function (res) {
                        productData = res;
                        for (let i = 0; i < productData.docs.length; i++) {
                            p = 0;
                            if (id === productData.docs[i]._id) {
                                if (parseInt(localStorage.getItem('whole-price')) > 0) {
                                    carts = JSON.parse(localStorage.getItem(`cart-array`));
                                }
                                if (parseInt(localStorage.getItem(`p-${i}`)) >= 0) {
                                    p = parseInt(localStorage.getItem(`p-${i}`)) + 1;
                                } else {
                                    p = 1;
                                }
                                price = parseInt(`${productData.docs[i].price}`);
                                whole_price += price;

                                let className = document.getElementById(`${slug}-clicked-` + t).className;
                                if (className === t + `-clicked-${slug}`) {
                                    document.getElementById(`${slug}-clicked-` + t).className = 'changed';
                                }

                                let description = productData.docs[i].mainDescription[l].content.split('/');
                                carts[(j - 1) * 10 + i] = {
                                    price: Number(productData.docs[i].price),
                                    whole_price: p * Number(productData.docs[i].price),
                                    count: p,
                                    img: serverApi + productData.docs[i].image.path,
                                    title: productData.docs[i].title[l].content,
                                    description: description[0],
                                    slug: sl
                                };
                                localStorage.setItem(`cart-array`, JSON.stringify(carts));
                                let js = JSON.parse(localStorage.getItem('cart-array'));
                                if (js !== null) {
                                    times = 0;
                                    for (let m = 0; m < js.length; m++) {
                                        if (js[m] !== null) {
                                            times++;
                                        }
                                    }
                                } else {
                                    times = 1;
                                }
                                localStorage.setItem(`p-${i}`, JSON.stringify(carts[(j - 1) * 10 + i].count));
                            }
                        }
                        localStorage.setItem('whole-price', whole_price);
                        localStorage.setItem('times', times);
                        document.getElementById('whole-price').innerHTML = whole_price;
                        document.getElementById('times').innerHTML = times;
                    }
                });
            }
        }
    });
}


function getPopupCart() {
    document.getElementById('popup-cart').style.display = "block";

}

function deletePopupCart() {
    $(document).ready(function () {
        getPopupSendCart();
    });
    document.getElementById('popup-cart').style.display = "none";
}


function getTypeProducts(slug) {
    setCookie('TypeProducts', slug, 30);
    setSearchParams('productCategory', slug);
    $(document).ready(function () {
        getProductData(slug);
    });
}

function getTranslate(lang, i) {
    console.log('lang:', lang)
    setCookie('lang', lang, 30);
    setCookie('lang-item', i, 30);
    let carts = JSON.parse(localStorage.getItem(`cart-array`));
    $.ajax({
        url: `http://api.coffeein.md/api/products`,
        type: 'Get',
        dataType: 'json',
        success: function (res) {
            productData = res;
            for (let m = 0; m < carts.length; m++) {
                if (carts[m] !== null) {
                    let description = productData.docs[m].mainDescription[i].content.split('/');
                    carts[m].title = productData.docs[m].title[i].content;
                    carts[m].description = description[0];
                }
            }
            localStorage.setItem(`cart-array`, JSON.stringify(carts));
        }
    })
    $.ajax({
        url: `../languages/${getCookie('lang')}.json`,
        type: 'Get',
        dataType: 'json',
        success: function (response) {
            localStorage.setItem('lang_array', JSON.stringify(response))
        }
    });
}

function setSearchParams(name, value) {
    const url = new URL(window.location.href);
    url.searchParams.set(name, value);
    window.location.href = url.href;
}


function getSearchParams(name) {
    const url = new URL(window.location.href);
    return url.searchParams.get(name);
}


function getLocation() {
    if (navigator.geolocation && document.querySelector('#share-location:checked') !== null) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        document.getElementById('map3').style.display = "none";
        document.getElementById('map2').style.display = "block";
    }
}

function showPosition(position) {
    document.getElementById('map2').style.display = "none";
    document.getElementById('map3').style.display = "block";
    var map = L.map('map3').setView([position.coords.latitude, position.coords.longitude], 10);

    L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);
    var greenIcon = L.icon({
        iconUrl: 'http://coffeein.md/assets/img/icons/map-marker.png',

        iconSize: [16, 16],
        popupAnchor: [0, -8]
    });
    L.marker([position.coords.latitude, position.coords.longitude], {icon: greenIcon}).addTo(map)
        .bindPopup('Va aflati aici').openPopup();
    console.log('latitude', position.coords.latitude);
    console.log('longitude', position.coords.longitude);

}

function getPopupSendCart() {
    let carts_price = parseInt(localStorage.getItem('whole-price'));
    let html = ``;
    html = `    <div class="cart-products">
    <div class="popup-cart" onclick="deletePopupCart()"></div>
        <div class="popup">
            <div class="popup-task">
                <h1>${changeLanguage.COMPLETE_ORDER_1}
                    <br>
                    ${changeLanguage.COMPLETE_ORDER_2}</h1>
            </div>
            <div class="contacts">
                <div class="number-popup">
                    <div class="popup-number-title">
                        <p>${changeLanguage.PHONE}   <span>(${changeLanguage.REQUIRED})</span></p>
                    </div>
                    <div class="write-number">
                        <div class="write-number-img">
                        <img src="http://coffeein.md/assets/img/icons/moldova-icon.svg"> +373
                        </div>
                        <input class="input-number" id="input-number" type="number" placeholder="0000-0000" maxlength="8"     oninput="javascript: if (this.value.length > this.maxLength) this.value = this.value.slice(0, this.maxLength);"
                        >
                    </div>
                </div>
                <div class="name-popup-title">
                    <p>${changeLanguage.NAME}</p>
                    <div class="write-name">
                        <input placeholder="${changeLanguage.NAME_PLACEHOLDER}" class="input-name" id="input-name" >
                    </div>
                </div>
            </div>

            <div class="total-popup">
                <h1>${changeLanguage.TOTAL}<b id="price">${carts_price}</b><sup>lei</sup></h1>
            </div>
            <div class="share-location">
                <input type="checkbox" id="share-location" name="share-location" onclick="getLocation()">
                <label for="share-location">${changeLanguage.SHARE_LOCATION}</label>
            </div>
            <div id="map2"></div>
            <div id="map3" style="display: none"></div>
            <button id="finish-popup" onclick="deletePopupCart()"><p>${changeLanguage.FINISH_ORDER}</p></button>
        </div>
    </div>`
    document.getElementById('popup-cart').innerHTML = html;
}

//Set and get Cookies
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}


//get product carts


function getProductCart() {
    let carts = JSON.parse(localStorage.getItem(`cart-array`));
    if (carts.length > 0) {
        for (let i = 0; i < carts.length; i++) {
            if (carts[i] !== null) {
                document.getElementById('products-cart').innerHTML += `
   <div  class="products-cart" id="cart-${i}">
<img src="${carts[i].img}">
        <div class="cart-description">
            <h1>${carts[i].title}</h1>
            <p>${carts[i].description}</p>
        </div>
        <div class="change-cart">
            <p class="cart-decrease" id="cart-decrease" onclick="getDecrease(${i})">-</p>
            <p class="cart-products-nr" id="${slug}-cart-products-nr-${i}">${carts[i].count}</p>
            <p class="cart-increase" id="cart-increase" onclick="getIncrease(${i})">+</p>
        </div>

        <div class="cart-price">
            <p><b  id="${slug}-cart-price-${i}">${carts[i].whole_price}</b><sup>LEI</sup></p>
        </div>

        <div class="cart-close" id="cart-close" onclick="getClose('cart-${i}', ${i}, '${carts[i].slug}')"><p>+</p></div></div>`;
            }
        }
    }
    document.getElementById(`slideshow-${slug}`).style.display = "block";
}


function getDecrease(t) {
    let carts = JSON.parse(localStorage.getItem(`cart-array`));
    let totalPrice = parseInt(localStorage.getItem('whole-price'));
    const product = document.getElementById(`${slug}-cart-products-nr-${t}`);
    const price = document.getElementById(`${slug}-cart-price-${t}`);
    allPrice = parseInt(price.innerText);
    products = parseInt(product.innerText);
    for (let i = 0; i < carts.length; i++) {
        if (i === t && products > 0) {

            totalPrice -= parseInt(carts[i].price);
            products--;
            carts[i].count = products;
            allPrice = products * parseInt(carts[t].price);
            document.getElementById(`price`).innerHTML = totalPrice.toString();
            carts[i].whole_price = allPrice;
        }
        product.innerHTML = products;
        price.innerHTML = allPrice;
        localStorage.setItem(`cart-array`, JSON.stringify(carts));
        localStorage.setItem(`whole-price`, JSON.stringify(totalPrice));
    }
    localStorage.setItem(`p-${t}`, JSON.stringify(products));
}

function getIncrease(t) {
    let carts = JSON.parse(localStorage.getItem(`cart-array`));
    let totalPrice = parseInt(localStorage.getItem('whole-price'));
    const product = document.getElementById(`${slug}-cart-products-nr-${t}`);
    const price = document.getElementById(`${slug}-cart-price-${t}`);
    allPrice = parseInt(price.innerText);
    products = parseInt(product.innerText);
    products++;
    allPrice = products * parseInt(carts[t].price);
    product.innerHTML = products;
    price.innerHTML = allPrice;
    for (let i = 0; i < carts.length; i++) {
        if (i === t) {
            carts[i].count = products;
            carts[i].whole_price = allPrice;
            totalPrice += parseInt(carts[i].price);
            document.getElementById(`price`).innerHTML = totalPrice.toString();
        }
        // document.getElementById('price').innerHTML=localStorage.getItem('whole-price');
        localStorage.setItem(`cart-array`, JSON.stringify(carts));
        localStorage.setItem(`whole-price`, JSON.stringify(totalPrice));
    }
    localStorage.setItem(`p-${t}`, JSON.stringify(products));

}

function getClose(cl, i) {
    document.getElementById(cl).style.display = 'none';
    let carts = JSON.parse(localStorage.getItem('cart-array'));
    let totalPrice = parseInt(localStorage.getItem('whole-price'));
    let times = parseInt(localStorage.getItem('times'))
    totalPrice -= parseInt(carts[i].whole_price);
    carts[i] = null;
    times--;
    localStorage.setItem(`whole-price`, JSON.stringify(totalPrice));
    localStorage.setItem('times', JSON.stringify(times));
    localStorage.setItem('cart-array', JSON.stringify(carts));
    localStorage.setItem(`p-${i}`, JSON.stringify(0));
    document.getElementById(`price`).innerHTML = totalPrice.toString();

}



